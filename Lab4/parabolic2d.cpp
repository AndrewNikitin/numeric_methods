#include "parabolic2d.h"

/// TODO: Logging files, Some refactoring

void Solver::TMAHorizontal(VecDouble &lower, VecDouble &diag, VecDouble &upper, VecDouble &rhs, Layer &layer, int col, int size)
{
    VecDouble alpha(size + 1);
    VecDouble beta(size + 1);

    alpha[1] = -upper[0] / diag[0];
    beta[1] = rhs[0] / diag[0];

    for (int i = 1; i < size; ++i)
    {
        double denominator = lower[i] * alpha[i] + diag[i];
        alpha[i + 1] = -upper[i] / denominator;
        beta[i + 1] = (rhs[i] - lower[i] * beta[i]) / denominator;
    }

    layer[size - 1][col] = beta[size];
    for (int i = size - 2; i >= 0; --i)
    {
        layer[i][col] = alpha[i + 1] * layer[i + 1][col] + beta[i + 1];
    }
}

void Solver::TMAVertical(VecDouble &lower, VecDouble &diag, VecDouble &upper, VecDouble &rhs, Layer &layer, int row, int size) {
    VecDouble alpha(size + 1);
    VecDouble beta(size + 1);

    alpha[1] = -upper[0] / diag[0];
    beta[1] = rhs[0] / diag[0];

    for (int i = 1; i < size; ++i) {
        double denominator = lower[i] * alpha[i] + diag[i];
        alpha[i + 1] = -upper[i] / denominator;
        beta[i + 1] = (rhs[i] - lower[i] * beta[i]) / denominator;
    }

    layer[row][size - 1] = beta[size];
    for (int i = size - 2; i >= 0; --i) {
        layer[row][i] = alpha[i + 1] * layer[row][i + 1] + beta[i + 1];

    }
}

void Solver::ADIMethod(Parabolic2DEquation &eq, int x_steps, int y_steps, int t_steps, double t_max,
                       std::function<double(double, double, double)> &solution, std::string &log)
{
    double hx = eq.l1 / x_steps;
    double hy = eq.l2 / y_steps;
    double hx2 = hx * hx;
    double hy2 = hy * hy;
    double tau = t_max / t_steps;

    Layer layer(x_steps + 1, VecDouble(y_steps + 1));
    Layer sublayer(x_steps + 1, VecDouble(y_steps + 1));

    for (int i = 0; i <= x_steps; ++i)
    {
        for (int j = 0; j <= y_steps; ++j)
        {
            layer[i][j] = eq.phi(hx * i, hy * j);
        }
    }

    int vec_size = std::max(x_steps, y_steps) + 1;
    VecDouble lower(vec_size);
    VecDouble upper(vec_size);
    VecDouble diag(vec_size);
    VecDouble rhs(vec_size);

    double max_error = 0.0;

    for (int k = 0; k < t_steps; ++k)
    {
        for (int i = 1; i < x_steps; ++i)
        {
            lower[i] = eq.a / hx2 - eq.b / (2 * hx);
            diag[i] = -2 * eq.a / hx2 - 2 / tau + eq.d;
            upper[i] = eq.a / hx2 + eq.b / (2 * hx);
        }

        for (int j = 1; j < y_steps; ++j)
        {
            for (int i = 1; i < x_steps; ++i)
            {
                rhs[i] =  -eq.f(hx * i, hy * j, tau * k + tau / 2);
                rhs[i] -= (eq.a / hy2 - eq.c / (2 * hy)) * layer[i][j - 1];
                rhs[i] -= (-2 * eq.a / hy2 + 2 / tau) * layer[i][j];
                rhs[i] -= (eq.a / hy2 + eq.c / (2 * hy)) * layer[i][j + 1];
            }

            diag[0] = eq.beta1 * hx - eq.alpha1;
            upper[0] = eq.alpha1;
            rhs[0] = hx * eq.gamma1(hy * j, tau * k + tau / 2);

            diag[x_steps] = eq.beta2 * hx + eq.alpha2;
            lower[x_steps] = -eq.alpha2;
            rhs[x_steps] = hx * eq.gamma2(hy * j, tau * k + tau / 2);

            TMAHorizontal(lower, diag, upper, rhs, sublayer, j, x_steps + 1);
        }

        for (int i = 0; i <= x_steps; ++i)
        {
            sublayer[i][0] = (- eq.alpha3 * sublayer[i][1] + hy * eq.gamma3(hx * i, tau * (k + 0.5))) / (eq.beta3 * hy - eq.alpha3);
            sublayer[i][y_steps] = (eq.alpha4 * sublayer[i][y_steps - 1] + hy * eq.gamma4(hx * i, tau * (k + 0.5))) / (eq.beta4 * hy + eq.alpha4);
        }

        for (int j = 1; j < y_steps; ++j)
        {
            lower[j] = eq.a / hy2 - eq.c / (2 * hy);
            diag[j] = -2 * eq.a / hy2 - 2/tau + eq.d;
            upper[j] = eq.a / hy2 + eq.c / (2 * hy);
        }

        for (int i = 1; i < x_steps; ++i)
        {
            for (int j = 1; j < y_steps; ++j)
            {
                rhs[j] = -eq.f(hx * i, hy * j, k * tau + tau);
                rhs[j] -= (eq.a / hx2 - eq.b / (2 * hx)) * sublayer[i - 1][j];
                rhs[j] -= (-2 * eq.a / hx2 + 2 / tau) * sublayer[i][j];
                rhs[j] -= (eq.a / hx2 + eq.b / (2 * hx)) * sublayer[i + 1][j];
            }

            diag[0] = eq.beta3 * hy - eq.alpha3;
            upper[0] = eq.alpha3;
            rhs[0] = hy * eq.gamma3(hx * i, (k + 1) * tau);

            diag[y_steps] = eq.beta4 * hy + eq.alpha4;
            lower[y_steps] = -eq.alpha4;
            rhs[y_steps] = hy * eq.gamma4(hx * i, (k + 1) * tau);

            TMAVertical(lower, diag, upper, rhs, layer, i, y_steps + 1);
        }

        for (int j = 0; j <= y_steps; ++j)
        {
            layer[0][j] = (- eq.alpha1 * layer[1][j] + hx * eq.gamma1(hy * j, tau * (k + 1))) / (eq.beta1 * hx - eq.alpha1);
            layer[x_steps][j] = (eq.alpha2 * layer[x_steps - 1][j] + hx * eq.gamma2(hy * j, tau * (k + 1))) / (eq.beta2 * hx + eq.alpha2);
        }

        for(int i = 0; i <= x_steps; ++i)
        {
            for(int j = 0; j <= y_steps; ++j)
            {
                double uij = solution(i * hx, j * hy, (k + 1) * tau);
                max_error = std::max(max_error, std::abs(layer[i][j] - uij));
            }
        }
    }
    std::cerr << "[ADI] Evaluation complete. Max abs error is " << max_error << std::endl;
}


void Solver::FSMethod(Parabolic2DEquation &eq, int x_steps, int y_steps, int t_steps, double t_max,
                      std::function<double (double, double, double)> &solution, std::string &log)
{
    double hx = eq.l1 / x_steps;
    double hy = eq.l2 / y_steps;
    double hx2 = hx * hx;
    double hy2 = hy * hy;
    double tau = t_max / t_steps;

    Layer layer(x_steps + 1, std::vector<double> (y_steps + 1));

    for (int i = 0; i <= x_steps; ++i)
    {
        for (int j = 0; j <= y_steps; ++j)
        {
            layer[i][j] = eq.phi(hx * i, hy * j);
        }
    }

    int vec_size = std::max(x_steps, y_steps) + 1;
    VecDouble lower(vec_size);
    VecDouble diag(vec_size);
    VecDouble upper(vec_size);
    VecDouble rhs(vec_size);

    double max_error = 0.0;

    for (int k = 0; k < t_steps; ++k)
    {
        for (int i = 1; i < x_steps; ++i)
        {
            lower[i] = eq.a / hx2 - eq.b / (2 * hx);
            diag[i] = -2 * eq.a / hx2 - 1 / tau + eq.d / 2;
            upper[i] = eq.a / hx2 + eq.b / (2 * hx);
        }

        for (int j = 0; j <= y_steps; ++j)
        {
            for (int i = 1; i < x_steps; ++i)
            {
                rhs[i] = -layer[i][j] / tau - eq.f(hx * i, hy * j, tau * k) / 2;
            }

            diag[0] = eq.beta1 * hx - eq.alpha1;
            upper[0] = eq.alpha1;
            rhs[0] = hx * eq.gamma1(hy * j, tau * k + tau);

            diag[x_steps] = eq.beta2 * hx + eq.alpha2;
            lower[x_steps] = -eq.alpha2;
            rhs[x_steps] = hx * eq.gamma2(hy * j, tau * k + tau);

            TMAHorizontal(lower, diag, upper, rhs, layer, j, x_steps + 1);
        }

        for (int j = 1; j < y_steps; ++j)
        {
            lower[j] = eq.a / hy2 - eq.c / (2 * hy);
            diag[j] = -2 * eq.a / hy2 - 1 / tau + eq.d / 2;
            upper[j] = eq.a / hy2 + eq.c / (2 * hy);
        }

        for (int i = 0; i <= x_steps; ++i)
        {
            for (int j = 1; j < y_steps; ++j)
            {
                rhs[j] = - layer[i][j] / tau - eq.f(hx * i, hy * j, tau * (k + 1)) / 2;
            }

            diag[0] = eq.beta3 * hy - eq.alpha3;
            upper[0] = eq.alpha3;
            rhs[0] = hy * eq.gamma3(hx * i, (k + 1) * tau);

            diag[y_steps] = eq.beta4 * hy + eq.alpha4;
            lower[y_steps] = -eq.alpha4;
            rhs[y_steps] = hy * eq.gamma4(hx * i, (k + 1) * tau);

            TMAVertical(lower, diag, upper, rhs, layer, i, y_steps + 1);
        }

        for(int i = 0; i <= x_steps; ++i)
        {
            for(int j = 0; j <= y_steps; ++j)
            {
                double uij = solution(i * hx, j * hy, (k + 1) * tau);
                max_error = std::max(max_error, std::abs(layer[i][j] - uij));
            }
        }
    }
    std::cerr << "[FS] Evaluation complete. Max abs error is " << max_error << std::endl;
}


/// TODO: Implement loading dump from file
void Solver::LoadLayerFromFile()
{

}

/// TODO: Implement dumping to file
void Solver::SaveLayerToFile()
{

}

void TestMethods()
{
    Parabolic2DEquation eq(
            1.0, 1.0, 1.0, 1.0, [](double x, double y, double t){ return - x * y * sin(t) - x * cos(t) - y * cos(t) - x * y * cos(t); },
            [](double x, double y){ return x * y;},
            0.0, 1.0, [](double y, double t){ return 0 /* y*cos(t) */; },
            0.0, 1.0, [](double y, double t){ return 1.0 * y*cos(t); },
            0.0, 1.0, [](double x, double t){ return 0 /* x*cos(t) */;},
            0.0, 1.0, [](double x, double t){ return 1.0 * x*cos(t); },
            1.0, 1.0
    );

    std::function<double(double, double, double)> solution([](double x, double y, double t){return x * y * cos(t);});
    std::string log_("sample.log");

    int x_steps, y_steps, t_steps;
    double t_max = 1.0;

    std::cin >> x_steps >> y_steps >> t_steps;

    Solver solver;
    solver.ADIMethod(eq, x_steps, y_steps, t_steps, t_max, solution, log_);
    solver.FSMethod(eq, x_steps, y_steps, t_steps, t_max, solution, log_);
}